#!/bin/bash
cd vidin
# convert all videos from mkv to mp4 for jumpcutter
for f in *.mkv; do ffmpeg -i "$f" -c copy "${f%.mkv}.mp4"; done
# remove all spaces from any video files
find . -name "* *" -type f | rename 's/ /_/g'
# start python script splitting videos int 1h files
echo "Splitting Videos...\n"
python3 vidsplit.py
# run python script jcbulk.py which shortens 1h chunks into faster, more dense videos
echo "Shortening Videos...\n"
screen -dmS jcbulk python3 jcbulk.py