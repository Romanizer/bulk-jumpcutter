#!/bin/bash
ifile=$1                 #inputfile:       avengers_264.mp4
framerate=$2
extension="${ifile##*.}" #just extenstion: mp4
filename="${ifile%.*}"   #file name:       avengers_264
#outfile="short/${filename}_short.${extension}"
outfile=$3
#screen -dmS jcse 
/usr/bin/python3 jc.py --input_file $ifile --output_file $outfile --sounded_speed 1.6 --silent_speed 8 --frame_margin 2 --frame_rate $framerate

