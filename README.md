# Bulk Jumpcutter

This assortment of scripts processes multiple large video files.
The video gets sped up, and silent parts (with no audio) get sped up even faster.
This tool is meant for shortening the time to sit through a whole bundle of lectures, etc...