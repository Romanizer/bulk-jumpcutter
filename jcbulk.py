import os, random, sys

#files = os.listdir("/home/rufus/programming/adsp/vidout")
#files.remove("short")

allfiles = list() # new list of all files
for (dirpath, dirnames, filenames) in os.walk(f"/home/rufus/programming/adsp/vidout"):
    allfiles += [os.path.join(dirpath, file) for file in filenames] # append all files with full path

for file in allfiles:
    filename = file.split("/")[-1]
    pathin = file.strip(filename)
    pathout= file.strip(filename).replace('vidout', 'shortout')
    print(f"Input file:  {file} {pathout}")
    #print(f"./jc.sh {pathin}{filename} 30 {pathout}{filename}")
    os.system(f"mkdir {pathout}")
    os.system(f"./jc.sh {pathin}{filename} 30 {pathout}{filename}")
