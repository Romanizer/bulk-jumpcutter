import os, random, sys

files = os.listdir("/home/rufus/programming/adsp/vidin")
#files.remove("short")

for file in files:
    print(f"Working on: vidin/{file}")
    os.system(f"mkdir vidout/{file}")
    os.system(f"ffmpeg -i vidin/{file} -c copy -map 0 -segment_time 01:00:00 -f segment -reset_timestamps 1 vidout/{file}/output%03d.mp4")
